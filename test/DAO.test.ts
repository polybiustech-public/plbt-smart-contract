import { expect } from "chai";
import { ethers, network } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Contract, BigNumber, BigNumberish } from "ethers";
import * as it from "mocha-steps";
import {
  DAO,
  ERC20,
  PLBTStaking,
  IUniswapV2Router02,
  IPool,
} from "../typechain";
import dotenv from "dotenv";
import fs from "fs";
import { getContractFactory } from "@nomiclabs/hardhat-ethers/types";
import { isObject } from "util";

const envConfig = dotenv.parse(fs.readFileSync(`.env-${network.name}`));
for (const parameter in envConfig) {
  process.env[parameter] = envConfig[parameter];
}

let dao: DAO;
let plbt: ERC20;
let wbtc: ERC20;
let weth: ERC20;
let staking: PLBTStaking;
let uniswap: IUniswapV2Router02;
let pool: IPool;

let wethHolder: SignerWithAddress;
let wbtcHolder: SignerWithAddress;
let treasury: SignerWithAddress;
let deployer: SignerWithAddress;
let users: SignerWithAddress[];

let snapshot: any;

let debatingPeriod = parseInt(process.env.DEBATING_PERIOD_DURATION as string);
let regularTimelock = parseInt(process.env.REGULAR_TIMELOCK as string);
let cancelTimelock = parseInt(process.env.CANCEL_TIMELOCK as string);

let votingPercent: BigNumber;

let modifier = 10 ** 4;

let voting_quorum = 450000;
let quorumABI = ["function changeQuorum(bool or, uint256 _quorum)"];
let iface = new ethers.utils.Interface(quorumABI);
let quorumData = iface.encodeFunctionData("changeQuorum", [1, voting_quorum]);

let cancelABI = ["function cancelVoting(uint256 id)"];
iface = new ethers.utils.Interface(cancelABI);
let cancelData = iface.encodeFunctionData("cancelVoting", [1]);

let majorityABI = ["function changeMajority(bool or, uint256 _majority)"];
iface = new ethers.utils.Interface(majorityABI);
let majorityData = iface.encodeFunctionData("changeMajority", [
  1,
  voting_quorum,
]);

let incorrectAbi = ["function changeMaaority(bool or, uint256 _majority)"];
iface = new ethers.utils.Interface(incorrectAbi);
let incorrectData = iface.encodeFunctionData("changeMaaority", [
  1,
  voting_quorum,
]);

let treasuryABI = ["function changeTreasury(address _treasury)"];
iface = new ethers.utils.Interface(treasuryABI);
let newTreasury = "0x037F37e29b8dBC4badC5f05CCc499625937efc9f";
let treasuryData = iface.encodeFunctionData("changeTreasury", [newTreasury]);

async function getCurrentTime() {
  let blockNumber = await ethers.provider.getBlockNumber();
  let block = await ethers.provider.getBlock(blockNumber);
  return block.timestamp;
}

async function deploy(deployTime: number) {
  [deployer, treasury, ...users] = await ethers.getSigners();

  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: ["0x2e50196a15777eb570894f292a4f5b1445ca5740"],
  });
  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: ["0xc9d14d0bd6a970b7854299a70e9cec6c52b6bee2"],
  });
  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: ["0x764aa0447cf441eb89a723eb21ed379706434f0a"],
  });
  await network.provider.send("hardhat_setBalance", [
    "0x2e50196a15777eb570894f292a4f5b1445ca5740",
    "0x8AC7230489E80000",
  ]);
  await network.provider.send("hardhat_setBalance", [
    "0xc9d14d0bd6a970b7854299a70e9cec6c52b6bee2",
    "0x8AC7230489E80000",
  ]);
  await network.provider.send("hardhat_setBalance", [
    "0x764aa0447cf441eb89a723eb21ed379706434f0a",
    "0x8AC7230489E80000",
  ]);

  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: ["0x2f0b23f53734252bda2277357e97e1517d6b042a"],
  });
  await network.provider.send("hardhat_setBalance", [
    "0x2f0b23f53734252bda2277357e97e1517d6b042a",
    "0x8AC7230489E80000",
  ]);

  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: ["0xbf72da2bd84c5170618fbe5914b0eca9638d5eb5"],
  });
  await network.provider.send("hardhat_setBalance", [
    "0xbf72da2bd84c5170618fbe5914b0eca9638d5eb5",
    "0x8AC7230489E80000",
  ]);

  let plbtHolders = [
    await ethers.getSigner("0x2e50196a15777eb570894f292a4f5b1445ca5740"),
    await ethers.getSigner("0xc9d14d0bd6a970b7854299a70e9cec6c52b6bee2"),
    await ethers.getSigner("0x764aa0447cf441eb89a723eb21ed379706434f0a"),
  ] as SignerWithAddress[];

  wethHolder = await ethers.getSigner(
    "0x2f0b23f53734252bda2277357e97e1517d6b042a"
  );
  wbtcHolder = await ethers.getSigner(
    "0xbf72da2bd84c5170618fbe5914b0eca9638d5eb5"
  );

  plbt = (await ethers.getContractAt(
    "ERC20",
    process.env.PLBT_TOKEN_ADDRESS as string
  )) as ERC20;
  weth = (await ethers.getContractAt(
    "ERC20",
    process.env.WETH_TOKEN_ADDRESS as string
  )) as ERC20;
  wbtc = (await ethers.getContractAt(
    "ERC20",
    process.env.WBTC_TOKEN_ADDRESS as string
  )) as ERC20;

  uniswap = (await ethers.getContractAt(
    "IUniswapV2Router02",
    process.env.SUSHISWAP_ROUTER_ADDRESS as string
  )) as IUniswapV2Router02;

  let PlbtStaking = await ethers.getContractFactory("PLBTStaking");
  staking = (await PlbtStaking.deploy(
    process.env.DISTRIBUTION_TIME
  )) as PLBTStaking;
  await staking.deployed();
  let Dao = await ethers.getContractFactory("DAO");
  dao = (await Dao.deploy(
    process.env.PROPOSAL_MAJORITY as string,
    process.env.VOTING_MAJORITY as string,
    process.env.PROPOSAL_QUORUM as string,
    process.env.VOTING_QUORUM as string,
    process.env.DEBATING_PERIOD_DURATION as string,
    process.env.REGULAR_TIMELOCK as string,
    process.env.CANCEL_TIMELOCK as string,
    [0, 0, 0, 0],
    [1, 1, 1, 1]
  )) as DAO;
  await dao.deployed();
  await staking.initialize(
    deployTime + 600,
    weth.address,
    wbtc.address,
    plbt.address,
    dao.address,
    treasury.address
  );
  await dao.initialize(
    process.env.SUSHISWAP_ROUTER_ADDRESS as string,
    treasury.address,
    staking.address,
    process.env.PLBT_TOKEN_ADDRESS as string,
    process.env.WETH_TOKEN_ADDRESS as string,
    process.env.WBTC_TOKEN_ADDRESS as string,
    process.env.GYSR_FACTORY_ADDRESS as string,
    process.env.GYSR_STAKING_MODULE_FACTORY_ADDRESS as string,
    process.env.GYSR_REWARD_MODULE_FACTORY_ADDRESS as string,
    process.env.SUSHI_LP_TOKEN_ADDRESS as string,
    process.env.OSOM_ADDRESS as string
  );
  pool = (await ethers.getContractAt("IPool", await dao.pool())) as IPool;
  await network.provider.send("evm_increaseTime", [601]);
  await network.provider.send("evm_mine");

  for (let i = 0; i < plbtHolders.length; i++) {
    await plbt
      .connect(plbtHolders[i])
      .transfer(
        plbtHolders[0].address,
        await plbt.balanceOf(plbtHolders[i].address)
      );
  }
  let votingQ = (await plbt.totalSupply()).div(33);
  votingPercent = (await plbt.totalSupply()).div(100).add(100000000);
  for (let i = 0; i < 10; i++) {
    await plbt.connect(plbtHolders[0]).transfer(users[i].address, votingQ);
    await plbt.connect(users[i]).approve(staking.address, votingQ);
    await staking.connect(users[i]).stake(votingQ);
  }

  await plbt
    .connect(plbtHolders[0])
    .transfer(treasury.address, await plbt.balanceOf(plbtHolders[0].address));
  await weth
    .connect(wethHolder)
    .transfer(treasury.address, await weth.balanceOf(wethHolder.address));
  await wbtc
    .connect(wbtcHolder)
    .transfer(treasury.address, await wbtc.balanceOf(wbtcHolder.address));
}
describe("participateInProposal tests", () => {
  it.step("deployment", async () => {
    await deploy(await getCurrentTime());
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao.getAllProposals())[0].id).to.be.equal(1);
  });

  it.step("STEP 0", async () => {
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await expect(
      dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        )
    ).to.be.revertedWith("addProposal: can't add more than 2 proposals");

    expect(await dao.getActiveProposals(users[0].address)).to.be.eq(2);
  });
});

describe("ACCESS TESTS", () => {
  it.step("Deployment", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
  });

  it.step(
    "STEP 1: should revert if onlyDAO function is called not by a DAO",
    async () => {
      await expect(dao.changeQuorum(true, 10)).to.be.revertedWith(
        "DAO: only dao can call this function."
      );
    }
  );

  it.step(
    "STEP 2: should revert if trying to initialize DAO again",
    async () => {
      await expect(
        dao.initialize(
          process.env.SUSHISWAP_ROUTER_ADDRESS as string,
          treasury.address,
          staking.address,
          plbt.address,
          weth.address,
          wbtc.address,
          process.env.GYSR_FACTORY_ADDRESS as string,
          process.env.GYSR_STAKING_MODULE_FACTORY_ADDRESS as string,
          process.env.GYSR_REWARD_MODULE_FACTORY_ADDRESS as string,
          process.env.SUSHI_LP_TOKEN_ADDRESS as string,
          process.env.OSOM_ADDRESS as string
        )
      ).to.be.revertedWith("DAO: Already initialized.");
    }
  );
});

describe("addProposal tests", () => {
  it.step("deployment", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
  });
  it.step("STEP 1: should revert if changesType = none", async () => {
    await expect(
      dao
        .connect(users[0])
        .addProposal(
          0,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        )
    ).to.be.revertedWith("DAO: addProposal bad arguments.");
  });

  it.step("STEP 2: should add proposal properly", async () => {
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao.getAllProposals())[0].id).to.be.equal(1);
  });

  it.step("STEP 3: should revert if proposal pool is full", async () => {
    for (let i = 0; i < 9; i++) {
      await dao
        .connect(users[i])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
    }
    await expect(
      dao
        .connect(users[1])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        )
    ).to.be.revertedWith("DAO: proposals list is full");
  });
});

describe("participateInProposal tests", () => {
  it.step("deployment", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao.getAllProposals())[0].id).to.be.equal(1);
  });

  it.step("STEP 1: should revert if voting has already ended", async () => {
    await network.provider.send("evm_increaseTime", [debatingPeriod + 1]);
    await network.provider.send("evm_mine");
    await expect(
      dao.connect(users[1]).participateInProposal(1, 10, true)
    ).to.be.revertedWith("DAO: proposal ended");
  });

  it.step("STEP 2: should revert if you have already voted", async () => {
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await network.provider.send("evm_increaseTime", [30]);
    await network.provider.send("evm_mine");
    await dao.connect(users[0]).participateInProposal(2, 10, false);
    expect((await dao.getAllProposals())[0].votesAgainst).to.be.equal(10);
    await expect(
      dao.connect(users[0]).participateInProposal(2, 10, true)
    ).to.be.revertedWith("DAO: you have already voted");
  });

  it.step(
    "STEP 3: should revert if amount exceeds your staked balance",
    async () => {
      await expect(
        dao
          .connect(users[1])
          .participateInProposal(2, ethers.utils.parseEther("100000"), true)
      ).to.be.revertedWith("DAO: incorrect amount");
    }
  );

  it.step("STEP 4: should vote correctly", async () => {
    await dao.connect(users[2]).participateInProposal(2, 50, true);
    expect((await dao.getAllProposals())[0].votesFor).to.be.equal(50);
  });
});

describe("Voting TESTS", () => {
  it.step("DEPLOYMENT and sending proposal to voting", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });

    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao.getAllProposals())[0].id).to.be.equal(1);
    await dao
      .connect(users[0])
      .participateInProposal(1, votingPercent.mul(2), false);
    await dao
      .connect(users[1])
      .participateInProposal(1, votingPercent.mul(3), true);
    expect((await dao["getActiveVoting()"]())[0].votesFor).to.be.equal(
      votingPercent.mul(3)
    );
    expect((await dao["getActiveVoting()"]())[0].status).to.be.equal(3);
  });

  it.step(
    "STEP 1: should revert if trying to vote with amount higher than staked",
    async () => {
      await expect(
        dao
          .connect(users[2])
          .participateInVoting(1, votingPercent.mul(100), false)
      ).to.be.revertedWith("DAO: incorrect amount");
    }
  );

  it.step("STEP 2: should vote correctly", async () => {
    await dao.connect(users[2]).participateInVoting(1, votingPercent, true);
    expect((await dao["getActiveVoting()"]())[0].votesFor.toString()).to.be.eq(
      votingPercent.mul(4)
    );
    await dao.connect(users[3]).participateInVoting(1, votingPercent, false);
    expect(
      (await dao["getActiveVoting()"]())[0].votesAgainst.toString()
    ).to.be.eq(votingPercent.mul(3));
  });

  it.step("STEP 3: should revert if you've already voted", async () => {
    await expect(
      dao.connect(users[0]).participateInVoting(1, votingPercent, false)
    ).to.be.revertedWith("DAO: you have already voted");
  });

  it.step("STEP 4: finish voting", async () => {
    for (let i = 4; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(1, votingPercent.mul(3), true);
    }
    await network.provider.send("evm_increaseTime", [
      debatingPeriod + regularTimelock + 100,
    ]);
    await network.provider.send("evm_mine");
    await dao.finishVoting(1);
    expect(await dao.votingQuorum()).to.be.eq(voting_quorum);
    expect((await dao.votings(1)).status).to.be.eq(2);
  });

  it.step(
    "STEP 5: should pick new proposal after voting is finished",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(2, votingPercent.mul(3), true);
      expect(
        (await dao["getActiveVoting()"]())[0].votesFor.toString()
      ).to.be.eq(votingPercent.mul(3));
    }
  );

  it.step("STEP 6: should revert if voting is already finished", async () => {
    await expect(
      dao.connect(users[1]).participateInVoting(1, votingPercent, false)
    ).to.be.revertedWith("DAO: voting ended");
  });

  it.step(
    "STEP 7: should revert if trying to finish voting too early",
    async () => {
      await expect(dao.finishVoting(2)).to.be.revertedWith(
        "DAO: Voting can't be finished yet."
      );
    }
  );

  it.step(
    "STEP 8: should revert if trying to finish finished voting",
    async () => {
      await expect(dao.finishVoting(1)).to.be.revertedWith(
        "DAO: the result of the vote has already been completed"
      );
    }
  );

  it.step(
    "STEP 9: should finish voting without implementation if voted against, then send next proposal to voting",
    async () => {
      await dao.connect(users[1]).participateInVoting(2, votingPercent, false);
      await dao.connect(users[2]).participateInVoting(2, votingPercent, false);
      await dao.connect(users[3]).participateInVoting(2, votingPercent, false);
      await network.provider.send("evm_increaseTime", [debatingPeriod + 1]);
      await network.provider.send("evm_mine");
      await expect(
        dao.connect(users[4]).participateInVoting(2, votingPercent, false)
      ).to.be.revertedWith("DAO: voting ended");
      await expect(dao.finishVoting(2)).to.be.revertedWith(
        "DAO: Voting can't be finished yet."
      );
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[4])
        .participateInProposal(3, votingPercent.mul(3), true);

      await network.provider.send("evm_increaseTime", [regularTimelock]);
      await network.provider.send("evm_mine");

      expect(await dao.finishVoting(2)).to.emit(dao, "Finished");
    }
  );

  it.step(
    "STEP 10: should revert if trying to vote with amount higher than available",
    async () => {
      await dao
        .connect(users[1])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[1])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[2])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[1])
        .participateInProposal(4, votingPercent, false);
      await dao
        .connect(users[1])
        .participateInProposal(5, votingPercent, false);
      await expect(
        dao
          .connect(users[1])
          .participateInProposal(6, votingPercent.mul(10), false)
      ).to.be.revertedWith("DAO: incorrect amount");
    }
  );

  it.step("STEP 11: getAvailableTokens", async () => {
    expect((await dao.getLockedTokens(users[1].address)).toString()).to.be.eq(
      votingPercent.mul(2)
    );
    expect(
      (await dao.getAvailableTokens(users[1].address)).toString()
    ).to.be.eq(
      (await staking.getStakedTokens(users[1].address)).sub(
        votingPercent.mul(2)
      )
    );
  });

  it.step(
    "STEP 12: should work properly with different changestypes",
    async () => {
      await network.provider.request({
        method: "evm_revert",
        params: [snapshot],
      });
      snapshot = await network.provider.request({
        method: "evm_snapshot",
      });
      await dao
        .connect(users[0])
        .addProposal(
          4,
          majorityData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      for (let i = 1; i < 10; i++) {
        await dao
          .connect(users[i])
          .participateInVoting(1, votingPercent.mul(3), true);
      }

      await network.provider.send("evm_increaseTime", [
        debatingPeriod + regularTimelock + 10,
      ]);
      await network.provider.send("evm_mine");
      await dao.finishVoting(1);
      expect(await dao.votingMajority()).to.be.eq(450000);

      await network.provider.request({
        method: "evm_revert",
        params: [snapshot],
      });
      snapshot = await network.provider.request({
        method: "evm_snapshot",
      });
      await dao
        .connect(users[0])
        .addProposal(
          5,
          treasuryData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      for (let i = 1; i < 10; i++) {
        await dao
          .connect(users[i])
          .participateInVoting(1, votingPercent.mul(3), true);
      }
      await network.provider.send("evm_increaseTime", [
        debatingPeriod + regularTimelock + 10,
      ]);
      await network.provider.send("evm_mine");
      await dao.finishVoting(1);
      expect(
        await dao.hasRole(await dao.TREASURY_ROLE(), newTreasury)
      ).to.be.eq(true);

      await network.provider.request({
        method: "evm_revert",
        params: [snapshot],
      });
      snapshot = await network.provider.request({
        method: "evm_snapshot",
      });
      await expect(
        dao
          .connect(users[0])
          .addProposal(
            4,
            incorrectData,
            "0x0000000000000000000000000000000000000000000000000000000000000001"
          )
      ).to.be.revertedWith("DAO: bytecode is wrong");
    }
  );
  it.step("STEP 13: finishAndAddProposal case", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        4,
        majorityData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[0])
      .participateInProposal(1, votingPercent.mul(3), true);
    expect((await dao.votings(1)).status).to.be.eq(3);
    await network.provider.send("evm_increaseTime", [debatingPeriod + 10]);
    await network.provider.send("evm_mine");
    await dao
      .connect(users[0])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[1])
      .participateInVoting(2, votingPercent.mul(3), true);
    expect((await dao.votings(2)).status).to.be.eq(3);
    await network.provider.send("evm_increaseTime", [debatingPeriod + 10]);
    await network.provider.send("evm_mine");
    for (let i = 2; i < 8; i++) {
      await dao
        .connect(users[i])
        .addProposal(
          4,
          majorityData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
    }
    await dao
      .connect(users[2])
      .participateInProposal(3, votingPercent.mul(3), true);
    await expect(dao.finishVoting(1)).to.be.revertedWith(
      "Voting can't be finished yet."
    );
    await dao
      .connect(users[0])
      .finishAndAddProposal(
        2,
        4,
        majorityData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao.votings(1)).status).to.be.eq(2);
    expect((await dao.votings(2)).status).to.be.eq(2);
  });

  it.step(
    "STEP 14: should pick next proposal and send it to voting if current voting is finished",
    async () => {
      await network.provider.request({
        method: "evm_revert",
        params: [snapshot],
      });
      snapshot = await network.provider.request({
        method: "evm_snapshot",
      });
      await dao
        .connect(users[0])
        .addProposal(
          4,
          majorityData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      await dao
        .connect(users[0])
        .addProposal(
          4,
          majorityData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[1])
        .participateInProposal(2, votingPercent.mul(3), true);
      expect((await dao.votings(1)).status).to.be.eq(3);
      await dao
        .connect(users[2])
        .participateInVoting(1, votingPercent.mul(3), true);
      await network.provider.send("evm_increaseTime", [
        debatingPeriod + regularTimelock + 100,
      ]);
      await network.provider.send("evm_mine");
      await dao.finishVoting(1);
      expect((await dao.votings(1)).status).to.be.eq(2);
      expect((await dao.votings(2)).status).to.be.eq(3);
    }
  );
});

describe("voting cancellation", () => {
  it.step("Deployment and voting creation", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[0])
      .participateInProposal(1, votingPercent.mul(3), true);
  });

  it.step("STEP 1: should be able to propose cancel vote", async () => {
    await network.provider.send("evm_increaseTime", [debatingPeriod]);
    await network.provider.send("evm_mine");
    await dao
      .connect(users[0])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: should be able to vote for proposal cancellation",
    async () => {
      for (let i = 1; i < 10; i++) {
        await dao
          .connect(users[i])
          .participateInVoting(2, votingPercent.mul(3), true);
      }
    }
  );

  it.step(
    "STEP 3: should revert cancellation proposal if voting cancellation is already in progress",
    async () => {
      await expect(
        dao
          .connect(users[1])
          .addProposal(
            6,
            cancelData,
            "0x0000000000000000000000000000000000000000000000000000000000000001"
          )
      ).to.be.revertedWith("Cancel Voting already exists");
    }
  );

  it.step("STEP 4: should cancel vote after finishing", async () => {
    await network.provider.send("evm_increaseTime", [debatingPeriod]);
    await network.provider.send("evm_mine");
    await dao.finishVoting(2);
    expect((await dao.votings(2)).status).to.be.eq(2);
    expect((await dao.votings(1)).status).to.be.eq(2);
    expect((await dao.votings(1)).implementationTime).to.be.eq(0);
    expect(await dao.votingQuorum()).to.be.eq(200000);
  });

  it.step("STEP 5: should revert if there is no active voting", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await expect(
      dao
        .connect(users[0])
        .addProposal(
          6,
          cancelData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        )
    ).to.be.revertedWith("DAO: Can't cancel twice.");
  });

  it.step(
    "STEP 6: should immediately finish voting successfully if cancellation has failed and the voting is winning",
    async () => {
      await network.provider.request({
        method: "evm_revert",
        params: [snapshot],
      });
      snapshot = await network.provider.request({
        method: "evm_snapshot",
      });
      await dao
        .connect(users[1])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      for (let i = 1; i < 7; i++) {
        await dao
          .connect(users[i])
          .participateInVoting(1, votingPercent.mul(3), true);
      }
      await network.provider.send("evm_increaseTime", [debatingPeriod + 100]);
      await network.provider.send("evm_mine");
      await dao
        .connect(users[2])
        .addProposal(
          6,
          cancelData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[7])
        .participateInVoting(2, votingPercent.mul(3), true);

      await network.provider.send("evm_increaseTime", [debatingPeriod + 100]);
      await network.provider.send("evm_mine");
      await dao.finishVoting(2);

      expect((await dao.votings(2)).status).to.be.eq(2);
      expect((await dao.votings(1)).status).to.be.eq(2);
      expect(await dao.votingQuorum()).to.be.eq(450000);
    }
  );

  it.step(
    "STEP 7: should immediately finish voting with a failure if cancellation has failed and the voting is losing",
    async () => {
      await network.provider.request({
        method: "evm_revert",
        params: [snapshot],
      });
      snapshot = await network.provider.request({
        method: "evm_snapshot",
      });
      await dao
        .connect(users[1])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      await dao
        .connect(users[1])
        .participateInVoting(1, votingPercent.mul(3).add(1), false);
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await dao
        .connect(users[1])
        .addProposal(
          6,
          cancelData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await dao.finishVoting(2);
      expect((await dao.votings(2)).status).to.be.eq(2);
      expect((await dao.votings(1)).status).to.be.eq(2);
      expect(await dao.votingQuorum()).to.be.eq(200000);
    }
  );
});

describe("View functions", () => {
  //TODO mostly a place holder
  it.step("DEPLOYMENT and sending proposal to voting", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao.connect(users[0]).participateInProposal(1, votingPercent, false);
    expect((await dao["getProposalInfo(uint256)"]("1"))[2]).to.be.eq(
      await dao.proposalQuorum()
    );
    await dao
      .connect(users[1])
      .participateInProposal(1, votingPercent.mul(3), true);
    expect((await dao["getProposalInfo(uint256)"]("1"))[2]).to.be.eq(
      await dao.votingQuorum()
    );
    for (let i = 2; i < 8; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(1, votingPercent.mul(3), true);
    }
    await network.provider.send("evm_increaseTime", [debatingPeriod]);
    await network.provider.send("evm_mine");
    await dao
      .connect(users[1])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );

    await network.provider.send("evm_increaseTime", [
      debatingPeriod + regularTimelock + 100,
    ]);
    await network.provider.send("evm_mine");
    expect(await dao.finishVoting(3)).to.emit(dao, "Finished");
    await dao
      .connect(users[1])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step("STEP 1: getVotingInfo(id)", async () => {
    await dao["getProposalInfo(uint256)"]("1");
  });

  it.step("STEP 2: getVotingInfo(id, user)", async () => {
    await dao["getProposalInfo(uint256,address)"]("1", users[0].address);
  });

  it.step("STEP 3: daoInfo", async () => {
    await dao.InfoDAO();
  });

  it.step("STEP 4: getActiveCancellation", async () => {
    expect((await dao["getActiveCancellation()"]())[0].status).to.be.eq(2);
  });
});

describe("Majority and Quorum tests", () => {
  let proposalQuorumPass: BigNumberish;
  let proposalQuorumFail: BigNumberish;
  let halfProposalQuorum: BigNumberish;
  beforeEach(async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });

    let totalSupply = await plbt.totalSupply();
    proposalQuorumPass = totalSupply.mul(3).div(100).add(100000000);
    proposalQuorumFail = totalSupply.mul(3).div(100).sub(100000000);
    halfProposalQuorum = totalSupply.mul(3).div(100).div(2);
  });
  it.step(
    "STEP 1: Shouldn't move proposal to voting if Quorum isn't reached",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, proposalQuorumFail, true);
      expect((await dao.votings(1)).status).to.be.eq(1);
    }
  );
  it.step(
    "STEP 2: Should move proposal to voting if Quorum and Majority are reached",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[0])
        .participateInProposal(1, proposalQuorumPass, true);
      expect((await dao.votings(1)).status).to.be.eq(3);
    }
  );

  it.step(
    "STEP 3: Shouldn't move proposal to voting if Quorum is reached, but Majority isn't",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      await dao
        .connect(users[1])
        .participateInProposal(1, halfProposalQuorum, false);
      await dao.connect(users[2]).participateInProposal(1, 15, false);
      await dao
        .connect(users[0])
        .participateInProposal(1, halfProposalQuorum, true);
      expect((await dao.votings(1)).status).to.be.eq(1);
    }
  );
});

describe("usecase tests for a single proposal", function () {
  it.step("STEP 1: deploy and add proposal", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: user0 participates in proposal with 1% against",
    async () => {
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent, false);
      await network.provider.send("evm_increaseTime", [150]);
      await network.provider.send("evm_mine");
      expect((await dao.getAllProposals())[0].votesAgainst).to.be.eq(
        votingPercent
      );
      await network.provider.send("evm_increaseTime", [100]);
      await network.provider.send("evm_mine");
    }
  );

  it.step("STEP 3: user1 participates in proposal with 3% for", async () => {
    await dao
      .connect(users[1])
      .participateInProposal(1, votingPercent.mul(3), true);
    expect((await dao.getAllProposals())[0].votesFor).to.be.eq(
      votingPercent.mul(3)
    );
  });

  it.step(
    "STEP 4: user2 can't participate in proposal since it's already a voting",
    async () => {
      expect((await dao.getAllProposals())[0].status).to.be.eq(3);
      await expect(
        dao
          .connect(users[2])
          .participateInProposal(1, votingPercent.mul(3), false)
      ).to.be.revertedWith("DAO: proposal ended");
    }
  );

  it.step("STEP 5: user2 participates in voting with 3% against", async () => {
    await dao
      .connect(users[2])
      .participateInVoting(
        await dao.activeVoting(),
        votingPercent.mul(3),
        false
      );
    expect((await dao["getActiveVoting()"]())[0].votesAgainst).to.be.eq(
      votingPercent.mul(4)
    );
  });

  it.step("STEP 6: user3 participates in voting with 3% for", async () => {
    await network.provider.send("evm_increaseTime", [250]);
    await network.provider.send("evm_mine");
    for (let i = 3; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(
          await dao.activeVoting(),
          votingPercent.mul(3),
          true
        );
    }
    expect((await dao["getActiveVoting()"]())[0].votesFor).to.be.eq(
      votingPercent.mul(24)
    );
  });

  it.step("STEP 7: can't finish the voting before endTime", async () => {
    await expect(dao.finishVoting(await dao.activeVoting())).to.be.revertedWith(
      "DAO: Voting can't be finished yet."
    );
  });

  it.step(
    "STEP 8: user4 can't participate in voting since it's voting period has already ended",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await expect(
        dao
          .connect(users[4])
          .participateInVoting(
            await dao.activeVoting(),
            votingPercent.mul(3),
            true
          )
      ).to.be.revertedWith("DAO: voting ended");
    }
  );

  it.step(
    "STEP 9: can't finish the voting after endTime but before finishTime",
    async () => {
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step(
    "STEP 10: should finish the voting after finishTime is reached",
    async () => {
      await network.provider.send("evm_increaseTime", [regularTimelock]);
      await network.provider.send("evm_mine");
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      await dao.finishVoting(await dao.activeVoting());
      expect(await dao.votingQuorum()).to.be.eq(voting_quorum);
    }
  );
});

describe("usecase tests for a single proposal with failed cancellation", function () {
  it.step("STEP 1: deploy and add proposal", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: user0 participates in proposal with 1% against",
    async () => {
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent, false);
      await network.provider.send("evm_increaseTime", [150]);
      await network.provider.send("evm_mine");
      expect((await dao.getAllProposals())[0].votesAgainst).to.be.eq(
        votingPercent
      );
      await network.provider.send("evm_increaseTime", [100]);
      await network.provider.send("evm_mine");
    }
  );

  it.step("STEP 3: user1 participates in proposal with 3% for", async () => {
    await dao
      .connect(users[1])
      .participateInProposal(1, votingPercent.mul(3), true);
    expect((await dao.getAllProposals())[0].votesFor).to.be.eq(
      votingPercent.mul(3)
    );
  });

  it.step(
    "STEP 4: user2 can't participate in proposal since it's already a voting",
    async () => {
      expect((await dao.getAllProposals())[0].status).to.be.eq(3);
      await expect(
        dao
          .connect(users[2])
          .participateInProposal(1, votingPercent.mul(3), false)
      ).to.be.revertedWith("DAO: proposal ended");
    }
  );

  it.step(
    "STEP 5: user2 participates in voting with 40000 against",
    async () => {
      await dao
        .connect(users[2])
        .participateInVoting(
          await dao.activeVoting(),
          votingPercent.mul(3),
          false
        );
      expect((await dao["getActiveVoting()"]())[0].votesAgainst).to.be.eq(
        votingPercent.mul(4)
      );
    }
  );

  it.step("STEP 6: user3 participates in voting with 50000 for", async () => {
    await network.provider.send("evm_increaseTime", [250]);
    await network.provider.send("evm_mine");
    for (let i = 3; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(
          await dao.activeVoting(),
          votingPercent.mul(3),
          true
        );
    }
    expect((await dao["getActiveVoting()"]())[0].votesFor).to.be.eq(
      votingPercent.mul(24)
    );
  });

  it.step("STEP 7: can't finish the voting before endTime", async () => {
    await expect(dao.finishVoting(await dao.activeVoting())).to.be.revertedWith(
      "DAO: Voting can't be finished yet."
    );
  });

  it.step(
    "STEP 8: user4 can't participate in voting since it's voting period has already ended",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await expect(
        dao
          .connect(users[4])
          .participateInVoting(
            await dao.activeVoting(),
            votingPercent.mul(3),
            true
          )
      ).to.be.revertedWith("DAO: voting ended");
    }
  );

  it.step(
    "STEP 9: can't finish the voting after endTime but before finishTime",
    async () => {
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step("STEP 10: adding cancellation voting", async () => {
    await network.provider.send("evm_increaseTime", [110]);
    await dao
      .connect(users[0])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao["getActiveVoting()"]())[0].wasCancelled).to.be.eq(true);
    expect((await dao["getActiveVoting()"]())[0].finishTime).to.be.eq(
      (await getCurrentTime()) + cancelTimelock
    );
  });

  it.step(
    "STEP 11: should finish the voting after finishTime is reached",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      await dao.finishVoting(await dao.activeCancellation());
      expect(await dao.votingQuorum()).to.be.eq(voting_quorum);
    }
  );
});

describe("usecase tests for a single proposal with winning cancellation", function () {
  it.step("STEP 1: deploy and add proposal", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: user0 participates in proposal with 1% against",
    async () => {
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent, false);
      await network.provider.send("evm_increaseTime", [150]);
      await network.provider.send("evm_mine");
      expect((await dao.getAllProposals())[0].votesAgainst).to.be.eq(
        votingPercent
      );
      await network.provider.send("evm_increaseTime", [100]);
      await network.provider.send("evm_mine");
    }
  );

  it.step("STEP 3: user1 participates in proposal with 3% for", async () => {
    await dao
      .connect(users[1])
      .participateInProposal(1, votingPercent.mul(3), true);
    expect((await dao.getAllProposals())[0].votesFor).to.be.eq(
      votingPercent.mul(3)
    );
  });

  it.step(
    "STEP 4: user2 can't participate in proposal since it's already a voting",
    async () => {
      expect((await dao.getAllProposals())[0].status).to.be.eq(3);
      await expect(
        dao
          .connect(users[2])
          .participateInProposal(1, votingPercent.mul(3), false)
      ).to.be.revertedWith("DAO: proposal ended");
    }
  );

  it.step(
    "STEP 5: user2 participates in voting with 40000 against",
    async () => {
      await dao
        .connect(users[2])
        .participateInVoting(
          await dao.activeVoting(),
          votingPercent.mul(3),
          false
        );
      expect((await dao["getActiveVoting()"]())[0].votesAgainst).to.be.eq(
        votingPercent.mul(4)
      );
    }
  );

  it.step("STEP 7: can't finish the voting before endTime", async () => {
    await expect(dao.finishVoting(await dao.activeVoting())).to.be.revertedWith(
      "DAO: Voting can't be finished yet."
    );
  });

  it.step(
    "STEP 8: user4 can't participate in voting since it's voting period has already ended",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await expect(
        dao
          .connect(users[4])
          .participateInVoting(
            await dao.activeVoting(),
            votingPercent.mul(3),
            true
          )
      ).to.be.revertedWith("DAO: voting ended");
    }
  );

  it.step(
    "STEP 9: can't finish the voting after endTime but before finishTime",
    async () => {
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step("STEP 10: adding cancellation voting", async () => {
    await network.provider.send("evm_increaseTime", [110]);
    await dao
      .connect(users[0])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );

    for (let i = 3; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(
          await dao.activeCancellation(),
          votingPercent.mul(3),
          true
        );
    }

    expect((await dao["getActiveVoting()"]())[0].wasCancelled).to.be.eq(true);
  });

  it.step(
    "STEP 13: should finish the voting after finishTime is reached",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      let vQ = await dao.votingQuorum();
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      await dao.finishVoting(await dao.activeCancellation());
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      expect(await dao.votingQuorum()).to.be.eq(vQ);
    }
  );
});

describe("distribute test", function () {
  it.step("STEP 1: call distribute function", async function () {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await plbt
      .connect(treasury)
      .approve(dao.address, await plbt.balanceOf(treasury.address));
    await weth
      .connect(treasury)
      .approve(dao.address, await weth.balanceOf(treasury.address));
    await wbtc
      .connect(treasury)
      .approve(dao.address, await plbt.balanceOf(treasury.address));

    let minAmount = (
      await uniswap.getAmountsOut(ethers.utils.parseEther("100"), [
        process.env.WETH_TOKEN_ADDRESS as string,
        process.env.PLBT_TOKEN_ADDRESS as string,
      ])
    )[1]
      .mul(97)
      .div(100);
    await dao
      .connect(treasury)
      .distribute(
        100,
        100,
        100,
        100,
        100,
        ethers.utils.parseEther("100"),
        minAmount
      );

    minAmount = (
      await uniswap.getAmountsOut(ethers.utils.parseEther("100"), [
        process.env.WETH_TOKEN_ADDRESS as string,
        process.env.PLBT_TOKEN_ADDRESS as string,
      ])
    )[1]
      .mul(97)
      .div(100);
    let frontrunnerAmounts = await uniswap.getAmountsOut(
      ethers.utils.parseEther("10000"),
      [
        process.env.WETH_TOKEN_ADDRESS as string,
        process.env.PLBT_TOKEN_ADDRESS as string,
      ]
    );
    await weth
      .connect(treasury)
      .approve(uniswap.address, await weth.balanceOf(treasury.address));
    await uniswap
      .connect(treasury)
      .swapTokensForExactTokens(
        frontrunnerAmounts[1] as BigNumber,
        frontrunnerAmounts[0],
        [
          process.env.WETH_TOKEN_ADDRESS as string,
          process.env.PLBT_TOKEN_ADDRESS as string,
        ],
        treasury.address,
        Math.floor(Date.now() / 1000) + 100
      );
    await expect(
      dao
        .connect(treasury)
        .distribute(
          100,
          100,
          100,
          100,
          100,
          ethers.utils.parseEther("100"),
          minAmount
        )
    ).to.be.revertedWith("DAO: amount of tokens is lower than expected");
  });
});

describe("gysr transfer test", function () {
  it.step("STEP 1: call transfer function", async function () {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao.connect(treasury).transferGYSROwnership(deployer.address);
    expect(await pool.controller()).to.be.eq(deployer.address);
    await dao.connect(treasury).transferGYSROwnership(dao.address);
    expect(await pool.controller()).to.be.eq(dao.address);
  });
});
describe("usecase tests for multiple proposals", function () {
  it.step("STEP 1: deploy and add proposal", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[1])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: user0 participates in proposal with 1% against",
    async () => {
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      await network.provider.send("evm_increaseTime", [150]);
      await network.provider.send("evm_mine");
      expect((await dao.getAllProposals())[0].votesFor).to.be.eq(
        votingPercent.mul(3)
      );
      await network.provider.send("evm_increaseTime", [100]);
      await network.provider.send("evm_mine");
    }
  );

  it.step(
    "STEP 3: users1/2 add a new proposal to the first slot and vote for both first and second proposal",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      expect(await dao.proposals(0)).to.be.eq(4);

      await dao
        .connect(users[1])
        .participateInProposal(2, votingPercent.mul(3), true);
      await dao
        .connect(users[2])
        .participateInProposal(2, votingPercent.mul(3), true);
    }
  );

  it.step(
    "STEP 4: user3 can't participate in proposal since it's already a voting",
    async () => {
      expect((await dao.votings(1)).status).to.be.eq(3);
      await expect(
        dao
          .connect(users[3])
          .participateInProposal(1, votingPercent.mul(3), false)
      ).to.be.revertedWith("DAO: proposal ended");
    }
  );

  it.step("STEP 5: user3 participates in voting with 3% against", async () => {
    await dao
      .connect(users[3])
      .participateInVoting(
        await dao.activeVoting(),
        votingPercent.mul(3),
        false
      );
    expect((await dao["getActiveVoting()"]())[0].votesAgainst).to.be.eq(
      votingPercent.mul(3)
    );
  });

  it.step("STEP 6: user4 participates in voting with 3% for", async () => {
    await network.provider.send("evm_increaseTime", [250]);
    await network.provider.send("evm_mine");
    await dao
      .connect(users[4])
      .participateInVoting(
        await dao.activeVoting(),
        votingPercent.mul(2),
        true
      );
    for (let i = 5; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(
          await dao.activeVoting(),
          votingPercent.mul(3),
          true
        );
    }
    expect((await dao["getActiveVoting()"]())[0].votesFor).to.be.eq(
      votingPercent.mul(20)
    );
  });

  it.step("STEP 7: can't finish the voting before endTime", async () => {
    await expect(dao.finishVoting(await dao.activeVoting())).to.be.revertedWith(
      "DAO: Voting can't be finished yet."
    );
  });

  it.step(
    "STEP 8: user4 can't participate in voting since it's voting period has already ended",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await expect(
        dao
          .connect(users[4])
          .participateInVoting(
            await dao.activeVoting(),
            votingPercent.mul(3),
            true
          )
      ).to.be.revertedWith("DAO: voting ended");
    }
  );

  it.step(
    "STEP 9: can't finish the voting after endTime but before finishTime",
    async () => {
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step(
    "STEP 10: should finish the voting after finishTime is reached",
    async () => {
      await network.provider.send("evm_increaseTime", [regularTimelock]);
      await network.provider.send("evm_mine");
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      await dao.finishVoting(await dao.activeVoting());
      expect(await dao.votingQuorum()).to.be.eq(voting_quorum);
    }
  );

  it.step("STEP 11: second proposal should be moved to voting", async () => {
    expect((await dao["getActiveVoting()"]())[0].id).to.be.eq(2);
    expect((await dao["getActiveVoting()"]())[0].status).to.be.eq(3);
  });
});
describe("usecase tests for multiple proposals with failed cancellation", function () {
  it.step("STEP 1: deploy and add proposal", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[1])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: user0 participates in proposal with 1% against",
    async () => {
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      await network.provider.send("evm_increaseTime", [150]);
      await network.provider.send("evm_mine");
      expect((await dao.getAllProposals())[0].votesFor).to.be.eq(
        votingPercent.mul(3)
      );
      await network.provider.send("evm_increaseTime", [100]);
      await network.provider.send("evm_mine");
    }
  );

  it.step(
    "STEP 3: users1/2 add a new proposal to the first slot and vote for both first and second proposal",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      expect(await dao.proposals(0)).to.be.eq(4);

      await dao
        .connect(users[1])
        .participateInProposal(2, votingPercent.mul(3), true);
      await dao
        .connect(users[2])
        .participateInProposal(2, votingPercent.mul(3), true);
    }
  );

  it.step(
    "STEP 4: user3 can't participate in proposal since it's already a voting",
    async () => {
      expect((await dao.votings(1)).status).to.be.eq(3);
      await expect(
        dao
          .connect(users[3])
          .participateInProposal(1, votingPercent.mul(3), false)
      ).to.be.revertedWith("DAO: proposal ended");
    }
  );

  it.step("STEP 5: user3 participates in voting with 3% against", async () => {
    await dao
      .connect(users[3])
      .participateInVoting(
        await dao.activeVoting(),
        votingPercent.mul(3),
        false
      );
    expect((await dao["getActiveVoting()"]())[0].votesAgainst).to.be.eq(
      votingPercent.mul(3)
    );
  });

  it.step("STEP 6: user4 participates in voting with 3% for", async () => {
    await network.provider.send("evm_increaseTime", [250]);
    await network.provider.send("evm_mine");
    await dao
      .connect(users[4])
      .participateInVoting(
        await dao.activeVoting(),
        votingPercent.mul(2),
        true
      );
    for (let i = 5; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(
          await dao.activeVoting(),
          votingPercent.mul(3),
          true
        );
    }
    expect((await dao["getActiveVoting()"]())[0].votesFor).to.be.eq(
      votingPercent.mul(20)
    );
  });

  it.step("STEP 7: can't finish the voting before endTime", async () => {
    await expect(dao.finishVoting(await dao.activeVoting())).to.be.revertedWith(
      "DAO: Voting can't be finished yet."
    );
  });

  it.step(
    "STEP 8: user4 can't participate in voting since it's voting period has already ended",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await expect(
        dao
          .connect(users[4])
          .participateInVoting(
            await dao.activeVoting(),
            votingPercent.mul(3),
            true
          )
      ).to.be.revertedWith("DAO: voting ended");
    }
  );

  it.step(
    "STEP 9: can't finish the voting after endTime but before finishTime",
    async () => {
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step("STEP 10: adding cancellation voting", async () => {
    await dao
      .connect(users[0])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao["getActiveVoting()"]())[0].wasCancelled).to.be.eq(true);
    expect((await dao["getActiveVoting()"]())[0].finishTime).to.be.eq(
      (await getCurrentTime()) + cancelTimelock
    );
  });

  it.step(
    "STEP 11: can't finish the voting after endTime but before changed finishTime",
    async () => {
      await network.provider.send("evm_increaseTime", [200]);
      await network.provider.send("evm_mine");
      await expect(
        dao.finishVoting(await dao.activeCancellation())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step(
    "STEP 12: should finish the voting after finishTime is reached",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      await dao.finishVoting(await dao.activeCancellation());
      expect(await dao.votingQuorum()).to.be.eq(voting_quorum);
    }
  );

  it.step("STEP 13: second proposal should be moved to voting", async () => {
    expect((await dao["getActiveVoting()"]())[0].id).to.be.eq(2);
    expect((await dao["getActiveVoting()"]())[0].status).to.be.eq(3);
  });
});

describe("usecase tests for multiple proposals with winning cancellation", function () {
  it.step("STEP 1: deploy and add proposal", async () => {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[0])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    await dao
      .connect(users[1])
      .addProposal(
        3,
        quorumData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
  });

  it.step(
    "STEP 2: user0 participates in proposal with 1% against",
    async () => {
      await dao
        .connect(users[0])
        .participateInProposal(1, votingPercent.mul(3), true);
      await network.provider.send("evm_increaseTime", [150]);
      await network.provider.send("evm_mine");
      expect((await dao.getAllProposals())[0].votesFor).to.be.eq(
        votingPercent.mul(3)
      );
      await network.provider.send("evm_increaseTime", [100]);
      await network.provider.send("evm_mine");
    }
  );

  it.step(
    "STEP 3: users1/2 add a new proposal to the first slot and vote for both first and second proposal",
    async () => {
      await dao
        .connect(users[0])
        .addProposal(
          3,
          quorumData,
          "0x0000000000000000000000000000000000000000000000000000000000000001"
        );
      expect(await dao.proposals(0)).to.be.eq(4);

      await dao
        .connect(users[1])
        .participateInProposal(2, votingPercent.mul(3), true);
      await dao
        .connect(users[2])
        .participateInProposal(2, votingPercent.mul(3), true);
    }
  );

  it.step(
    "STEP 4: user3 can't participate in proposal since it's already a voting",
    async () => {
      expect((await dao.votings(1)).status).to.be.eq(3);
      await expect(
        dao
          .connect(users[3])
          .participateInProposal(1, votingPercent.mul(3), false)
      ).to.be.revertedWith("DAO: proposal ended");
    }
  );
  it.step("STEP 5: can't finish the voting before endTime", async () => {
    await expect(dao.finishVoting(await dao.activeVoting())).to.be.revertedWith(
      "DAO: Voting can't be finished yet."
    );
  });

  it.step(
    "STEP 6: user4 can't participate in voting since it's voting period has already ended",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      await expect(
        dao
          .connect(users[4])
          .participateInVoting(
            await dao.activeVoting(),
            votingPercent.mul(3),
            true
          )
      ).to.be.revertedWith("DAO: voting ended");
    }
  );

  it.step(
    "STEP 7: can't finish the voting after endTime but before finishTime",
    async () => {
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step("STEP 8: adding cancellation voting", async () => {
    await dao
      .connect(users[0])
      .addProposal(
        6,
        cancelData,
        "0x0000000000000000000000000000000000000000000000000000000000000001"
      );
    expect((await dao["getActiveVoting()"]())[0].wasCancelled).to.be.eq(true);
    await network.provider.send("evm_increaseTime", [100]);
    await network.provider.send("evm_mine");
    await network.provider.send("evm_increaseTime", [100]);
    await network.provider.send("evm_mine");
  });

  it.step(
    "STEP 9: can't finish the voting after endTime but before changed finishTime",
    async () => {
      await network.provider.send("evm_increaseTime", [200]);
      await network.provider.send("evm_mine");
      await expect(
        dao.finishVoting(await dao.activeVoting())
      ).to.be.revertedWith("DAO: Voting can't be finished yet.");
    }
  );

  it.step("STEP 12: two users vote for cancellation", async () => {
    for (let i = 3; i < 10; i++) {
      await dao
        .connect(users[i])
        .participateInVoting(
          await dao.activeCancellation(),
          votingPercent.mul(3),
          true
        );
    }
    await expect(
      dao.finishVoting(await dao.activeCancellation())
    ).to.be.revertedWith("DAO: Voting can't be finished yet.");
  });

  it.step(
    "STEP 13: should finish the voting after finishTime is reached",
    async () => {
      await network.provider.send("evm_increaseTime", [debatingPeriod]);
      await network.provider.send("evm_mine");
      let vQ = await dao.votingQuorum();
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      await dao.finishVoting(await dao.activeCancellation());
      expect(await dao.votingQuorum()).to.be.not.eq(voting_quorum);
      expect(await dao.votingQuorum()).to.be.eq(vQ);
    }
  );

  it.step("STEP 14: second proposal should be moved to voting", async () => {
    expect((await dao["getActiveVoting()"]())[0].id).to.be.eq(2);
    expect((await dao["getActiveVoting()"]())[0].status).to.be.eq(3);
  });
});

describe("staking test", function () {
  it.step("STEP 1: staking", async function () {
    await network.provider.request({
      method: "evm_revert",
      params: [snapshot],
    });
    snapshot = await network.provider.request({
      method: "evm_snapshot",
    });
    await plbt
      .connect(treasury)
      .approve(dao.address, await plbt.balanceOf(treasury.address));
    await weth
      .connect(treasury)
      .approve(dao.address, await weth.balanceOf(treasury.address));
    await wbtc
      .connect(treasury)
      .approve(dao.address, await plbt.balanceOf(treasury.address));

    await dao
      .connect(treasury)
      .distribute(
        ethers.utils.parseUnits("100", "18"),
        ethers.utils.parseUnits("10", "8"),
        0,
        0,
        0,
        0,
        0
      );

    console.log("Expected 0 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
    );

    await network.provider.send("evm_increaseTime", [
      parseInt(process.env.DISTRIBUTION_TIME as string)/2,
    ]);
    await network.provider.send("evm_mine");
    console.log("Expected 5 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
    );
    await network.provider.send("evm_increaseTime", [
      parseInt(process.env.DISTRIBUTION_TIME as string)/2 - 3600,
    ]);
    await network.provider.send("evm_mine");

    console.log("Expected 10 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
    );

    await dao.connect(treasury).distribute(0, 0, 0, 0, 0, 0, 0);
    for (let i = 0; i < 5; i++) {
      await staking.connect(users[i]).claim();
    }

    console.log("Expected 10 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
    );

    await network.provider.send("evm_increaseTime", [
      parseInt(process.env.DISTRIBUTION_TIME as string)/2,
    ]);
    await network.provider.send("evm_mine");
    console.log("Expected 10 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
    );
    await network.provider.send("evm_increaseTime", [
      parseInt(process.env.DISTRIBUTION_TIME as string)/2 - 3600,
    ]);
    await network.provider.send("evm_mine");

    console.log("Expected 10 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
    );

    await dao
      .connect(treasury)
      .distribute(
        ethers.utils.parseUnits("50", "18"),
        ethers.utils.parseUnits("20", "6"),
        0,
        0,
        0,
        0,
        0
      );
      console.log("Expected 10 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
      );
  
      await network.provider.send("evm_increaseTime", [
        parseInt(process.env.DISTRIBUTION_TIME as string)/2,
      ]);
      await network.provider.send("evm_mine");
      console.log("Expected 12.5 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
      );
      await network.provider.send("evm_increaseTime", [
        parseInt(process.env.DISTRIBUTION_TIME as string)/2 - 3600,
      ]);
      await network.provider.send("evm_mine");
  
      console.log("Expected 15 weth " + 
      (await staking.getInfoByAddress(users[7].address)).claimWETH_.toString()
      );
    for (let i = 0; i < 10; i++) {
      await staking.connect(users[i]).claim();
    }
    await network.provider.send("evm_increaseTime", [100]);
  });
});
