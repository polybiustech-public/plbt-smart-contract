import fs from 'fs';
import dotenv from 'dotenv';
import {ethers} from 'hardhat';
import {Contract} from "ethers";
import hre from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

// gather deployment info
const network = hre.network.name;
const envfilePath = `.env-${network}`
const envConfig = dotenv.parse(fs.readFileSync(envfilePath));
for (const parameter in envConfig) {
    process.env[parameter] = envConfig[parameter]
}


async function main() {
    let owner: SignerWithAddress;
    let dao: Contract;


    [owner] = await ethers.getSigners();
    console.log("Owner address: ", owner.address)
    const balance = await owner.getBalance();
    console.log(`Owner account balance: ${ethers.utils.formatEther(balance).toString()}`)

    let allocation =  [42, 10, 42, 6]
    let strategy = [60, 0, 40, 0]
    
    //Deploy DAO Contract
    const DAO = await ethers.getContractFactory("DAO");
    dao = await DAO.deploy(
        process.env.PROPOSAL_MAJORITY as string,
        process.env.VOTING_MAJORITY as string,
        process.env.PROPOSAL_QUORUM as string,
        process.env.VOTING_QUORUM as string,
        process.env.DEBATING_PERIOD_DURATION as string,
        process.env.REGULAR_TIMELOCK as string,
        process.env.CANCEL_TIMELOCK as string,
        allocation,
        strategy
    );
    await dao.deployed();
    console.log(`DAO contract deployed to ${dao.address}`)

    let pool = await dao.pool();
    let rewardModule = await dao.gysr();
    
    //Sync env file
    fs.appendFileSync(`.env-${network}`, 
    `DAO_CONTRACT_ADDRESS=${dao.address}\r`)
}
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
