import fs from 'fs';
import dotenv from 'dotenv';
import {ethers} from 'hardhat';
import {Contract} from "ethers";
import hre from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

// gather deployment info
const network = hre.network.name;
const envfilePath = `.env-${network}`
const envConfig = dotenv.parse(fs.readFileSync(envfilePath));
for (const parameter in envConfig) {
    process.env[parameter] = envConfig[parameter]
}


async function main() {
    let owner: SignerWithAddress;
    let staking: Contract;

    [owner] = await ethers.getSigners();
    console.log("Owner address: ", owner.address)
    const balance = await owner.getBalance();
    console.log(`Owner account balance: ${ethers.utils.formatEther(balance).toString()}`)
    const time = Math.floor(Date.now() / 1000);
    //Deploy Staking
    const Staking = await ethers.getContractFactory(process.env.STAKING_NAME as string)
    console.log(
        process.env.DISTRIBUTION_TIME as string,        
    )
    staking = await Staking.deploy(
        process.env.DISTRIBUTION_TIME as string,
    )
    await staking.deployed()
    console.log(`Staking deployed to ${staking.address}`)

    //Sync env file
    fs.appendFileSync(`.env-${network}`, 
    `STAKING_CONTRACT_ADDRESS=${staking.address}\r`)
}
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
