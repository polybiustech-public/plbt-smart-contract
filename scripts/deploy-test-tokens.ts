import fs from 'fs';
import dotenv from 'dotenv';
import {ethers} from 'hardhat';
import {Contract} from "ethers";
import hre from 'hardhat';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';

// gather deployment info
const network = hre.network.name;
const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
for (const parameter in envConfig) {
    process.env[parameter] = envConfig[parameter]
}

async function main() {
    let owner: SignerWithAddress;
    let plbt : Contract;
    let weth: Contract;
    let wbtc: Contract;

    [owner] = await ethers.getSigners();
    console.log("Owner address: ", owner.address)
    const balance = await owner.getBalance();
    console.log(`Owner account balance: ${ethers.utils.formatEther(balance).toString()}`)

    //Deploy staking token
    const PLBT = await ethers.getContractFactory(process.env.PLBT_TOKEN_SYMBOL as string);
    
    plbt = await PLBT.deploy(
        process.env.PLBT_TOKEN_NAME,
        process.env.PLBT_TOKEN_SYMBOL,
        ethers.utils.parseEther(process.env.PLBT_TOKEN_MINT as string)
    );
    await plbt.deployed()
    console.log(`PLBT deployed to ${plbt.address}`)


    //Deploy reward tokens
    const wETH = await ethers.getContractFactory(process.env.WETH_TOKEN_SYMBOL as string);
    const wBTC = await ethers.getContractFactory(process.env.WBTC_TOKEN_SYMBOL as string);
    weth = await wETH.deploy(
        process.env.WETH_TOKEN_NAME,
        process.env.WETH_TOKEN_SYMBOL,
        ethers.utils.parseEther(process.env.WETH_TOKEN_MINT as string)
    );
    await weth.deployed()
    console.log(`wETH deployed to ${weth.address}`)
    wbtc = await wBTC.deploy(
        process.env.WBTC_TOKEN_NAME,
        process.env.WBTC_TOKEN_SYMBOL,
        ethers.utils.parseEther(process.env.WBTC_TOKEN_MINT as string)
    );
    await wbtc.deployed()
    console.log(`wBTC deployed to ${wbtc.address}`)

    //Sync env file
    fs.appendFileSync(`.env-${network}`, 
    `\r\# Deployed at \rPLBT_TOKEN_ADDRESS=${plbt.address}\rWETH_TOKEN_ADDRESS=${weth.address}\rWBTC_TOKEN_ADDRESS=${wbtc.address}\r`)
}
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });