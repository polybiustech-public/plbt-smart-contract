# Plbt Smart Contract

# Osom DApp
Osom Dapp is decentralized autonomous organization based on smart contracts and Ethereum.  Main goal is to provide robust DeFi services for PLBT holders and encourage others to become PLBT holders and PLBT Liquidity providers. Osom DAO is managed by PLBT stakers, which are able to create proposals and vote for or against changes.

**Glossary:**
- Liquidity Pool: PLBT/ETH pool created on SushiSwap. Users can become Liquidity providers by transfering ETH and PLBT to that pool on SushiSwap app https://app.sushi.com/pool and get tokens of PLBT/ETH pair (LP tokens) in return.

- LP Staking Contract: created on GYSR platform https://app.gysr.io/distribute through interface or by invoking create function on GYSR PoolFactory contract. Liquidity providers can stake their LP tokens in order to earn rewards in PLBT tokens. Total amount of rewards and distribution time is managed by DAO contract via proposals.

- PLBT Staking Contract: used for staking PLBT tokens. PLBT stakers earn rewards in wETH and wBTC in 50/50 ratio. Amount of rewards managed by DAO contract via proposals.

- DAO Contract: contains functionality for creating, finishing or canceling ongoing proposal. There could be only one active proposal at the time. Only PLBT Stakers can create proposals and vote with their staked PLBT tokens on PLBT Staking Contract. Staked PLBT tokens used for creating or voting are locked on Staking Contractg until proposal finishes. 

- Rewards: distributed monthly by treasury holders. Treasury hoders can be changed by creating proposals on DAO contract.

**Specification and Mechanics:**
- Proposals: entity of DAO which used to change strategy and funds allocation throughout Osom Dapp. 
    - Thresholds: are vital restrictions in proposal lifetime. In order to be created, become valid, and eventually finish proposal has two requirements to satisfy. 
        - Creating proposal: to create a proposal PLBT staker must have staked on the PLBT Staking Contract for at least N% of total PLBT amount , where N is managed by proposals and is initially set to 3%.
        - Quorum: to become formal, amount of votes(staked PLBT tokens used as voting power) proposal must pass M% of total PLBT amount during voting period, where M is managed by proposals and is initially set to 20%.
    - Timelock: there are two types of timelocks for proposals. 
        -72 hours Timelock: is a hold after voting period, which creates opportunity to learn and discuss purposes of proposal or cancel proposal.
        -14 days Timelock: comes in play after Cancel proposal function is invoked and becomes a voting period to cancel proposal and persuade others to vote for cancellation.
    - Cancel proposal: can be invoked for each regular active proposal during 72 hours Timelock. Thresholds for creating and voting such proposal are the same as for a regular proposal.

- Fund allocation: number of parameters creating a proportion, based on which Treasury distributes funds to LP Staking Contract, PLBT Staking Contract, OSOM, BuyBack and Burn. Parameters are present on DAO Contract and public (everyone can see them) and can be changed only by DAO Contract via proposals. 
    - Initial fund allocation parameters:
        - PLBT Stakers - 40%
        - LP Stakers - 6%
        - OSOM - 14%
        - Buyback - 40%
    - Parameters listed above are managed by DAO contract via proposals.

- Buyback and burn: mechanic which is used to buy PLBT back from SushiSwap pool and burn. Since there is no burn function on PLBT token Contract, nor methods to decrease total amount of PLBT tokens, DAO contract will be sending PLBT tokens to address(0), which will make these token unreachable for anyone in a blockchain. Hence we calculate total amount of PLBT tokens according to formula -  totalAmount  = plbt.totalAmount() - plbt.balanceOf(address(0)), where balanceOf(address(0)) retrieves amount of unreachable tokens.

**Development:**
PLBT_TOKEN_ADDRESS - 0x2145F63Cc6f1E6bdF61BF15580EAE6C140dB61b4
WETH_TOKEN_ADDRESS - 0x66dC04ED722D6bD0e4472418e156c2Ca34aA6dfd
WBTC_TOKEN_ADDRESS - 0x6fC9bC178A7dBCe6301FA1789798F4F1983BCc30
STAKING_CONTRACT_ADDRESS - 0xc4eD7faA8c2fc9c2615439A5b964C411B9745077
DAO_CONRACT_ADDRESS - 0x05B6806FEf9BdFB37E23847822F770e7CC54455d
SUSHI_LP_TOKEN_ADDRESS - 0x7cb4BB6D31be69a6aEd9041174b9d1744081308d
GYSR_POOL_ADDRESS - 0xaa9d14da6813d8483158c88061383894c9359328
