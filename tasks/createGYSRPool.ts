import fs from 'fs';
import {task} from "hardhat/config";
import dotenv from 'dotenv';


task("createGYSRPool", "create pool on GYSR factory")
    .setAction(async function (args, hre) {

        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }

        const gysrFactory = await hre.ethers.getContractAt("PoolFactory" ,process.env.GYSR_FACTORY_ADDRESS as string);
        
        let input = {
            staking: {
                types: ['address'], 
                values: [process.env.SUSHI_LP_TOKEN_ADDRESS as string]
            },
            reward: {
                types: ['address', 'uint256', 'uint256'], 
                values: [process.env.PLBT_TOKEN_ADDRESS as string, process.env.GYSR_PENALTY_START_TIME as string, process.env.GYSR_PENALTY_PERIOD as string]
            }
        }
        console.log(
            `Creating GYSR Pool with:\n
            ${process.env.GYSR_STAKING_MODULE_FACTORY_ADDRESS}\n
            ${process.env.GYSR_REWARD_MODULE_FACTORY_ADDRESS}\n
            ${process.env.SUSHI_LP_TOKEN_ADDRESS}\n
            ${process.env.PLBT_TOKEN_ADDRESS}, 
            ${process.env.GYSR_PENALTY_START_TIME}, 
            ${process.env.GYSR_PENALTY_PERIOD}`
        )
        const TX = await gysrFactory.create(
            process.env.GYSR_STAKING_MODULE_FACTORY_ADDRESS as string,
            process.env.GYSR_REWARD_MODULE_FACTORY_ADDRESS as string,
            hre.ethers.utils.defaultAbiCoder.encode(input.staking.types, input.staking.values),
            hre.ethers.utils.defaultAbiCoder.encode(input.reward.types, input.reward.values)
        )
        let txEvents: { event: string, args: { user: string, pool: string } }[] = (await TX.wait()).events;
        let address = (txEvents.find(txEvent => txEvent.event == "PoolCreated")?.args.pool) as string;
        console.log(`Deployed at: ${address}`);
        //Sync env file
        fs.appendFileSync(`.env-${network}`,
            `\nGYSR_POOL_ADDRESS=${address}\r`)
    });