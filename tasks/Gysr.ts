import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Contract } from "ethers";
import {task} from "hardhat/config";
import fs from 'fs';
import dotenv from 'dotenv';


task("gysr", "reward gysr")
    .setAction(async function (args, hre) {
        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }
    let me: SignerWithAddress,
        gysrPool: Contract,
        gysrRewardModule: Contract,
        gysrStakingModule: Contract,
        time = Math.floor(Date.now() / 1000);
        [me] = await hre.ethers.getSigners();
        gysrPool = await hre.ethers.getContractAt("Pool", process.env.GYSR_POOL_ADDRESS as string);
        gysrRewardModule = await hre.ethers.getContractAt("ERC20FriendlyRewardModule", process.env.GYSR_REWARD_MODULE_ADDRESS as string);
        gysrStakingModule = await hre.ethers.getContractAt("ERC20StakingModule", process.env.GYSR_STAKING_MODULE_ADDRESS as string);
        const vesting = await gysrRewardModule.timeVestingCoefficient(time);
        const rps = await gysrRewardModule.rewardsPerStakedShare();
        const stake = await gysrRewardModule.stakes(me.address, 0);
        const reward = (((rps - stake.rewardTally)*stake.shares)*vesting)/(10**39)
        console.log(
        `vesting: ${vesting.toString()}\r
        rps: ${rps.toString()}\r
        stake: ${stake.rewardTally}\r
        reward: ${reward.toString()}`);
    })