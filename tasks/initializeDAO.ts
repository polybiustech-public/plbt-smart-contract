import fs from "fs";
import { task } from "hardhat/config";
import dotenv from "dotenv";

task("initializeDAO", "Initialize DAO contract").setAction(async function (
  args,
  hre
) {
  const network = hre.network.name;
  const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
  for (const parameter in envConfig) {
    process.env[parameter] = envConfig[parameter];
  }

  const dao = await hre.ethers.getContractAt(
    process.env.DAO_NAME as string,
    process.env.DAO_CONTRACT_ADDRESS as string
  );

  await dao.initialize(
    process.env.SUSHISWAP_ROUTER_ADDRESS as string,
    process.env.TREASURY_ADDRESS as string,
    process.env.STAKING_CONTRACT_ADDRESS as string,
    process.env.PLBT_TOKEN_ADDRESS as string,
    process.env.WETH_TOKEN_ADDRESS as string,
    process.env.WBTC_TOKEN_ADDRESS as string,
    process.env.GYSR_FACTORY_ADDRESS as string,
    process.env.GYSR_STAKING_MODULE_FACTORY_ADDRESS as string,
    process.env.GYSR_REWARD_MODULE_FACTORY_ADDRESS as string,
    process.env.SUSHI_LP_TOKEN_ADDRESS as string,
    process.env.OSOM_ADDRESS as string
  );
  const pool = await hre.ethers.getContractAt("IPool", await dao.pool());

  fs.appendFileSync(`.env-${network}`, `GYSR_POOL_ADDRESS=${pool.address}\r`);
  fs.appendFileSync(
    `.env-${network}`,
    `GYSR_REWARD_MODULE_ADDRESS=${await pool.rewardModule()}\r`
  );
  fs.appendFileSync(
    `.env-${network}`,
    `GYSR_STAKING_MODULE_ADDRESS=${await pool.stakingModule()}\r`
  );
});
