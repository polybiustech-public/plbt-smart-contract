import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Contract } from "ethers";
import {task} from "hardhat/config";
import fs from 'fs';
import dotenv from 'dotenv';
import { isAddress } from "@ethersproject/address";

task("setDaoForTests", "get staking events")
    .setAction(async function (args, hre) {
        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }
    let me: SignerWithAddress,
        dao: Contract;
    [me] = await hre.ethers.getSigners();
    let inputs ={
            types: ["uint256","uint256","uint256","uint256"],
            values: ["10","20","30","40"]
        };
        dao = await hre.ethers.getContractAt(process.env.DAO_NAME as string , process.env.DAO_CONRACT_ADDRESS as string);
        let props = [
            {id: 1, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 0, creator: me.address, passed: false,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 2, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 3, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 4, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 5, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 0, creator: me.address, passed: false,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 6, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 7, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 8, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 9, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 0, creator: me.address, passed: false,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 10, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 11, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 12, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 13, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 0, creator: me.address, passed: false,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 14, votesFor: 10_000_000, votesAgainst: 50_000, startTime: 1630971971, endTime: 1631490371, implementationTime: 1631602038, creator: me.address, passed: true,  proposalType: 2, status: 2, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)},
            {id: 15, votesFor: hre.ethers.utils.parseEther('46000000'), votesAgainst: hre.ethers.utils.parseEther('23000000'), startTime: 1630971971, endTime: 1631845803, implementationTime: 0, creator: "0x037F37e29b8dBC4badC5f05CCc499625937efc9f", passed: false,  proposalType: 6, status: 1, data: hre.ethers.utils.defaultAbiCoder.encode(inputs.types, inputs.values)}
        ]

        let del = [
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator: "0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []},
            {id: 0, votesFor: hre.ethers.utils.parseEther('0'), votesAgainst: hre.ethers.utils.parseEther('0'), startTime: 0, endTime: 0, implementationTime: 0, creator:"0x0000000000000000000000000000000000000000", passed: false,  proposalType: 0, status: 0, data: []}
        ]
        // let admin = hre.ethers.utils.id("ADMIN_ROLE");
        // await dao.grantRole(admin, me.address)
        await dao.setProposals(del);
        //await dao.setCount(16);
        // await dao.setAllocation({plbtStakers: 20, osomStakers: 14, lpStakers: 16, buyback: 50})
    })