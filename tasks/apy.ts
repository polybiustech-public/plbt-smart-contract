import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Contract } from "ethers";
import {task} from "hardhat/config";
import fs from 'fs';
import dotenv from 'dotenv';
import { ethers } from "hardhat";


task("apy", "calculate apy")
    .setAction(async function (args, hre) {
        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }
    let me: SignerWithAddress,
        router: Contract,
        staking: Contract;
    
        router = await hre.ethers.getContractAt("IUniswapV2Router02", process.env.SUSHISWAP_ROUTER_ADDRESS as string);
        staking = await hre.ethers.getContractAt(process.env.STAKING_NAME as string, process.env.STAKING_CONTRACT_ADDRESS as string);
        let info = await staking.getStakingInfo(),
            reward = info._rewardTotal,
            staked = info._totalStaked,
            [plbt, weth, wbtc] = await router.getAmountsOut(hre.ethers.utils.parseEther((10).toString()), [process.env.PLBT_TOKEN_ADDRESS as string, process.env.WETH_TOKEN_ADDRESS as string,process.env.WBTC_TOKEN_ADDRESS as string]),
            proportion = hre.ethers.BigNumber.from(plbt).mul(2).div(hre.ethers.BigNumber.from(weth).add(wbtc)),
            apy = hre.ethers.BigNumber.from(reward).mul(proportion).mul(100).div(staked);
            console.log(plbt.toString(),"\n", weth.toString(),"\n", wbtc.toString())
            console.log(apy.toString())

    })