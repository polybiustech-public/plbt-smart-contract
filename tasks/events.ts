import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { Contract } from "ethers";
import {task} from "hardhat/config";
import fs from 'fs';
import dotenv from 'dotenv';


task("events", "get staking events")
    .setAction(async function (args, hre) {
        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }
    let me: SignerWithAddress,
        staking: Contract;
        [me] = await hre.ethers.getSigners();
        staking = await hre.ethers.getContractAt(process.env.STAKING_NAME as string , process.env.STAKING_CONTRACT_ADDRESS as string);
        let eventFilter = staking.filters.Claimed(null, null, null, me.address);
        let events = await staking.queryFilter(eventFilter);
        console.log(events[0]);

    })