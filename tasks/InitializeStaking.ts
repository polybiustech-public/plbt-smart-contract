import fs from 'fs';
import {task} from "hardhat/config";
import dotenv from 'dotenv';

task("initializeStaking", "Initialize Staking contract with tokens")
    .setAction(async function (args, hre) {

        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }

        const time = Math.floor(Date.now() / 1000);

        const staking = await hre.ethers.getContractAt(process.env.STAKING_NAME as string, process.env.STAKING_CONTRACT_ADDRESS as string);
        
        await staking.initialize(
            time+600,
            process.env.WETH_TOKEN_ADDRESS,
            process.env.WBTC_TOKEN_ADDRESS,
            process.env.PLBT_TOKEN_ADDRESS,
            process.env.DAO_CONTRACT_ADDRESS,
            process.env.TREASURY_ADDRESS as string
            );
        console.log(
            `Staking Contract Initialized with Tokens:\n
            ${process.env.WETH_TOKEN_SYMBOL} -  ${process.env.WETH_TOKEN_ADDRESS}\n
            ${process.env.WBTC_TOKEN_SYMBOL} -  ${process.env.WBTC_TOKEN_ADDRESS}\n
            ${process.env.PLBT_TOKEN_SYMBOL} -  ${process.env.PLBT_TOKEN_ADDRESS}`
        );
    });