import {task} from "hardhat/config";
import fs from 'fs';
import dotenv from 'dotenv';


task("createSushiSwapPool", "create PLBT/ETH pool on Sushiswap")
    .setAction(async function (args, hre) {
        const network = hre.network.name;
        const envConfig = dotenv.parse(fs.readFileSync(`.env-${network}`));
        for (const parameter in envConfig) {
            process.env[parameter] = envConfig[parameter]
        }
        const [owner] = await hre.ethers.getSigners();
        const plbt = await hre.ethers.getContractAt(process.env.PLBT_TOKEN_SYMBOL as string, process.env.PLBT_TOKEN_ADDRESS as string);
        const weth = await  hre.ethers.getContractAt(process.env.WETH_TOKEN_SYMBOL as string, process.env.WETH_TOKEN_ADDRESS as string)
        const sushiRouter = await hre.ethers.getContractAt("IUniswapV2Router02", process.env.SUSHISWAP_ROUTER_ADDRESS as string);
        let deadline = Math.floor(Date.now()/1000) + 600;
        let amountWETH = hre.ethers.utils.parseEther("100");
        let amountPLBT = hre.ethers.utils.parseEther("100000");
        console.log(
            `Creating PLBT-WETH Liquidity Pool with:\n
            ${plbt.address}\n
            ${weth.address}\n
            ${amountWETH}\n
            ${amountPLBT}\n
            0\n
            0\n
            ${owner.address}\n
            ${deadline}\n`
        );
        await plbt.approve(sushiRouter.address, amountPLBT);
        await weth.approve(sushiRouter.address, amountWETH);
        await sushiRouter.addLiquidity(
           plbt.address,
           weth.address,
           amountPLBT,
           amountPLBT,
           0,
           0,
           owner.address,
           deadline
        )
        const factory = await hre.ethers.getContractAt("IUniswapV2Factory", process.env.SUSHISWAP_FACTORY_ADDRESS as string);
        const pair = await factory.getPair(plbt.address, weth.address);
        console.log("Created at: ", pair);
        //Sync env file
        fs.appendFileSync(`.env-${network}`, 
        `SUSHI_LP_TOKEN_ADDRESS=${pair.address}\r`)
    })